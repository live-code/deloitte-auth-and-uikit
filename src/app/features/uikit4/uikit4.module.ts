import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { Uikit4RoutingModule } from './uikit4-routing.module';
import { Uikit4Component } from './uikit4.component';


@NgModule({
  declarations: [
    Uikit4Component
  ],
  imports: [
    CommonModule,
    Uikit4RoutingModule,
    SharedModule
  ]
})
export class Uikit4Module { }
