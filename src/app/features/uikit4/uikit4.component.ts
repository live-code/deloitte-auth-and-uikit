import { AfterViewInit, Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PanelComponent } from '../../shared/components/accordion/panel.component';
import { CardComponent } from '../../shared/components/card/card.component';

@Component({
  selector: 'app-uikit4',
  template: `
 
    <app-fx gap="lg">
      <div>1</div>
      <div>2</div>
      <div>3</div>
      <div>4</div>
    </app-fx>
    
    <!--BOOTSTRAP EXAMPLE  -->
    <app-row mq="sm">
      <app-col>
        <app-row mq="md">
          <app-col>A</app-col>
          <app-col>B</app-col>
          <app-col>C</app-col>
        </app-row>
      </app-col>
      <app-col>2</app-col>
      <app-col>3</app-col>
    </app-row>
    
    
    <app-accordion>
     <app-panel title="one">
       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda dicta earum non sint. Ad animi delectus dignissimos dolorum inventore libero minima molestiae numquam odit perspiciatis, quasi repellat, tempora tempore!
       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda dicta earum non sint. Ad animi delectus dignissimos dolorum inventore libero minima molestiae numquam odit perspiciatis, quasi repellat, tempora tempore!
       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda dicta earum non sint. Ad animi delectus dignissimos dolorum inventore libero minima molestiae numquam odit perspiciatis, quasi repellat, tempora tempore!
       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda dicta earum non sint. Ad animi delectus dignissimos dolorum inventore libero minima molestiae numquam odit perspiciatis, quasi repellat, tempora tempore!
       <app-leaflet [coords]="[43, 13]" />
     </app-panel>
  
     <app-panel title="two">
       <app-leaflet [coords]="[43, 13]" />
     </app-panel>
     
     <app-panel title="three">
       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda dicta earum non sint. Ad animi delectus dignissimos dolorum inventore libero minima molestiae numquam odit perspiciatis, quasi repellat, tempora tempore!
       <app-leaflet [coords]="[43, 13]" />
     </app-panel>
    </app-accordion>
`,

})
export class Uikit4Component {

}
