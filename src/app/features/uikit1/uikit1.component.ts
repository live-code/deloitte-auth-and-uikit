import { Component } from '@angular/core';

@Component({
  selector: 'app-uikit1',
  template: `
  
    <h1>UIKIT</h1>
    
    
    <app-card 
      title="Profile"
      variant="failed"
      icon="fa fa-facebook"
      (iconClick)="openUrl()"
    >
      <app-card-body>
        bla bla body
      </app-card-body>
      
      <app-card-footer>
        footerrrrr 2023
      </app-card-footer>
    </app-card>

    
    <app-card
      title="Profile"
      icon="fa fa-google"
      variant="failed"
      [(isOpened)]="isOpened"
    >
      <app-card-body>
        bla bla body
      </app-card-body>

      <app-card-footer>
        footerrrrr 2023
      </app-card-footer>
    </app-card>
    
    <pre>{{isOpened}}</pre>

  `,
})
export class Uikit1Component {
  isOpened = true;

  openUrl() {
    console.log('open url!')
  }
}
