import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from '../../shared/components/card/card.module';
import { SharedModule } from '../../shared/shared.module';

import { Uikit1RoutingModule } from './uikit1-routing.module';
import { Uikit1Component } from './uikit1.component';


@NgModule({
  declarations: [
    Uikit1Component
  ],
  imports: [
    CommonModule,
    Uikit1RoutingModule,
    SharedModule,
  ]
})
export class Uikit1Module { }
