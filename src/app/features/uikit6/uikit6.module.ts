import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { Uikit6RoutingModule } from './uikit6-routing.module';
import { Uikit6Component } from './uikit6.component';


@NgModule({
  declarations: [
    Uikit6Component
  ],
  imports: [
    CommonModule,
    Uikit6RoutingModule,
    SharedModule
  ]
})
export class Uikit6Module { }
