import { Component, TemplateRef, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from '../../core/authentication.service';
import { CardComponent } from '../../shared/components/card/card.component';
import { LeafletComponent } from '../../shared/components/leaflet.component';

export type WidgetType = 'card' | 'leaflet'
export type WidgetList = CardComponent | LeafletComponent

export const COMPONENTS_MAP: { [key: string]: Type<WidgetList> } = {
  'card': CardComponent,
  'leaflet': LeafletComponent
}

export interface Config {
  type: WidgetType;
  data: any;
  events?: string[]
}

@Component({
  selector: 'app-uikit6',
  template: `
    
    <h1>Directives</h1>
    
    <h2>Custom Repeaters</h2>
    <li 
      *repeater="let user of users; let i = index; let tot = total"
    >
      {{i}}. {{user.name}} {{tot}}
    </li>

    <li
      *usersRepeater="let user of users; let i = index; let tot = total"
    >
      {{i}}. {{user.displayName}} {{tot}}
    </li>

    <h2>Dynamic component loader</h2>
    
    <div
      *ngFor="let c of config"
      [appLoader]="c"
      (doAction)="doAction($event)"
    ></div>
    
    
    <hr>
    <h2>IsLogged  & Role Directives</h2>
    
    <button *appIsLogged>Admin 1 features</button>
    <button *appIfRoleIs="'admin'">Admin 2 features</button>
    <button *appIfRoleIs="'guest'">Guest features</button>
    
  `,
})
export class Uikit6Component {

  config: Config[] = [
    {
      type: 'leaflet',
      data: {
        coords: [42,13],
        zoom: 10
      },
    },
    {
      type: 'card',
      data: {
        title: 'Profile',
        isOpened: true,
        icon: 'fa fa-times'
      },
      events: [
        'iconClick',
        'isOpenedChange'
      ]
    }
  ];

  doAction(params: any) {
    console.log(params)
  }

  users: User[] = [
    { id: 1, name: 'Mario', surname: 'Rossi' },
    { id: 2, name: 'Lorenzo', surname: 'Verdi' },
    { id: 3, name: 'Fabio', surname: 'Biondi' },
    { id: 4, name: 'Fabio', surname: 'Biondi' },
  ];

}



export type User = {
  id: number;
  name: string;
  surname: string
}
