import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit6Component } from './uikit6.component';

const routes: Routes = [{ path: '', component: Uikit6Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit6RoutingModule { }
