import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit7PipesComponent } from './uikit7-pipes.component';

const routes: Routes = [{ path: '', component: Uikit7PipesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit7PipesRoutingModule { }
