import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { Uikit7PipesRoutingModule } from './uikit7-pipes-routing.module';
import { Uikit7PipesComponent } from './uikit7-pipes.component';


@NgModule({
  declarations: [
    Uikit7PipesComponent
  ],
  imports: [
    CommonModule,
    Uikit7PipesRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class Uikit7PipesModule { }
