import { Component } from '@angular/core';
import { formatDistance } from 'date-fns';

@Component({
  selector: 'app-uikit7-pipes',
  template: `
      <h1>Pipes Demo</h1>
      
      
      <h3>In to Array</h3>
      
      <li *ngFor="let item of items">
        {{item.name}} - Voto: {{item.rate}}
        
        <span 
          *ngFor="let star of item.rate | intToArray"
        >️️️️️️⭐️</span>
        
      </li>
      
      
      <h3>Times Ago</h3>
      {{1646217006000 | date: 'MM/YY'}}
      {{1646217006000 | timesago }}
      {{1687765578000 | timesago }}
      
      <h3>Double</h3>
      {{value | double}}
      
      <h3>Memory</h3>
      {{value | memory: 'mb'}}
      {{value | memory: 'byte'}}

      <h3>Mapquest</h3>
      <img [src]="city | mapquest: null : 300" alt="">
      <img [src]="'Palermo' | mapquest: 300 : 300" alt="">
      
  `,
})
export class Uikit7PipesComponent {
  value = 10;
  city = 'Milan'

  items = [
    { name: 'item 1', rate: 3},
    { name: 'item 2', rate: 6},
    { name: 'item 3', rate: 4},
  ]

}
