import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { filter, Subscription } from 'rxjs';
import { AuthenticationService } from '../../core/authentication.service';

@Component({
  selector: 'app-login',
  template: `
    <h1>Login</h1>
    <input type="text">
    <input type="text">
    <button (click)="authSrv.login('fab', '123')">SIGNIN</button>
  `,
})
export class LoginComponent implements OnDestroy {
  sub: Subscription;

  constructor(
    public authSrv: AuthenticationService,
    private router: Router
  ) {

    this.sub = this.authSrv.isLogged$
      .pipe(
       filter(isLogged => isLogged)
      )
      .subscribe(isLogged => {
       // this.router.navigateByUrl('home')
      })

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
