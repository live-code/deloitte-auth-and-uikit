import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { Uikit8PipesRoutingModule } from './uikit8-pipes-routing.module';
import { Uikit8PipesComponent } from './uikit8-pipes.component';


@NgModule({
  declarations: [
    Uikit8PipesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    Uikit8PipesRoutingModule,
    FormsModule
  ]
})
export class Uikit8PipesModule { }
