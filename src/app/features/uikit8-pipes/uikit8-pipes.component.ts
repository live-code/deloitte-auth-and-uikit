import { Component } from '@angular/core';

@Component({
  selector: 'app-uikit8-pipes',
  template: `
    <h1>Pipes with ngFor</h1>
    
    <input type="text" [(ngModel)]="filterName">
    
    <select [(ngModel)]="filterGender">
      <option value="all">Select All</option>
      <option value="M">Male</option>
      <option value="F">Female</option>
    </select>
    
    
    <select [(ngModel)]="sortValue">
      <option value="ASC">ASC</option>
      <option value="DESC">DESC</option>
    </select>
    
    
    <li *ngFor="let user of users | filterByGender: filterGender 
                                  | filterByName: filterName
                                  | sort: sortValue"
    >
      
      {{user.name}} ({{user.age}}) - {{user.gender}}
    </li>
  `,
})
export class Uikit8PipesComponent {
  filterGender: Gender = 'all'
  filterName: string = ''
  sortValue: 'ASC' | 'DESC' = 'ASC';

  users: User[] = [
    { id: 1, name: 'Michele', age: 30, gender: 'M' },
    { id: 3, name: 'Silvia', age: 20, gender: 'F' },
    { id: 2, name: 'Mich', age: 25, gender: 'F' }
  ];

}


export interface User {
  id: number;
  name: string;
  age: number;
  gender: Gender;
}

export type Gender =  'M' | 'F' | 'all';
