import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit8PipesComponent } from './uikit8-pipes.component';

const routes: Routes = [{ path: '', component: Uikit8PipesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit8PipesRoutingModule { }
