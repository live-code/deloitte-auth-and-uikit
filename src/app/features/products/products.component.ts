import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { catchError, map, mergeAll, mergeMap, of } from 'rxjs';
import { AuthenticationService } from '../../core/authentication.service';

@Component({
  selector: 'app-products',
  template: `
    <h1>Products</h1>
    
    <div *ngIf="error">AHIA!</div>
    
    <li *ngFor="let p of products">
      {{p.name}}
    </li>
  `,
})
export class ProductsComponent {
  products: any[] = [];
  error = false;

  constructor(private http: HttpClient) {
    // ----------xo
    this.http.get<any[]>('http://localhost:3000/productsX')
      .subscribe({
        next: (res) => {
        //  console.log('SUCCESS', res)
          this.products = res
        },
        error: (err) => {
        //  console.log('COMPO ERROR!!!')
          this.error = true
        }
      })
  }
}
