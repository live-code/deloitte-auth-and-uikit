import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { share } from 'rxjs';

@Component({
  selector: 'app-home',
  template: `
    
    <h1>Home</h1>
    
   
    <!--
    <pre>{{users$ | async}}</pre>
    
    <li *ngFor="let user of users$ | async">
      {{user.name}}
    </li>-->
    
    <button (click)="loadData()">LOAD</button>
  `,
})
export class HomeComponent {
  users$ = this.http.get<any[]>('https://jsonplaceholder.typicode.com/users')
    .pipe(
      share()
    )

  constructor(private http: HttpClient) {}

  // -------------o|
  loadData() {
    this.users$
      .subscribe({
        next: (val) => console.log(val),
        error: (err) => console.log('errre'),
        complete: () => console.log('complete')
      })
  }
}
