import { Component } from '@angular/core';

export interface Country {
  id: number;
  name: string;
  desc: string;
  cities: City[]
}

export interface City {
  id: number;
  label: string;
}

@Component({
  selector: 'app-uikit2',
  template: `
    <h1>UIkit 2</h1>

    <app-tabbar 
      [items]="countries"
      [selectedItem]="selectedCountry"
      (tabClick)="countryClickHandler($event)"
    ></app-tabbar>

    <app-tabbar
      *ngIf="selectedCountry"
      [items]="selectedCountry.cities"
      labelField="city"
      [selectedItem]="selectedCity"
      (tabClick)="cityClickHandler($event)"
    ></app-tabbar>

    <img
      width="100%"
      *ngIf="selectedCity"
      [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center='+ selectedCity.label + '&size=400,300&zoom=10'"
      alt=""
    >

    <pre>{{selectedCity | json}}</pre>
  `,
})
export class Uikit2Component {
  countries: Country[] = [];
  selectedCountry: Country | undefined;
  selectedCity: City | undefined

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 2,
          name: 'germany',
          desc: 'bla bla 2',
          cities: [
            { id: 1, label: 'Berlin' },
            { id: 2, label: 'Monaco' }
          ]
        },
        {
          id: 1, name: 'italy', desc: 'bla bla 1',   cities: [
            { id: 11, label: 'Rome' },
            { id: 22, label: 'Milan' },
            { id: 33, label: 'Palermo' },
          ]
        },
        {
          id: 3, name: 'spain', desc: 'bla bla 3',
          cities: [
            {id: 41, label: 'Madrid'}
          ]},
      ];

      this.countryClickHandler(this.countries[0])
    }, 200)
  }

  countryClickHandler(country: Country) {
    this.selectedCountry = country;
    this.selectedCity = country.cities[0];
  }

  cityClickHandler(city: City) {
    this.selectedCity = city
  }
}
