import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { Uikit3RoutingModule } from './uikit3-routing.module';
import { Uikit3Component } from './uikit3.component';


@NgModule({
  declarations: [
    Uikit3Component
  ],
  imports: [
    CommonModule,
    Uikit3RoutingModule,
    SharedModule
  ]
})
export class Uikit3Module { }
