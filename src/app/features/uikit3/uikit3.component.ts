import { Component } from '@angular/core';

@Component({
  selector: 'app-uikit3',
  template: `
    
    <app-leaflet 
      [coords]="coords"
      [zoom]="zoomValue"
    />

    <button (click)="coords = [43, 13]">coords 1</button>
    <button (click)="coords = [45, 13]">coords 2</button>
    <button (click)="zoomValue = zoomValue - 1">-</button>
    <button (click)="zoomValue = zoomValue + 1">+</button>
  `,
})
export class Uikit3Component {

  zoomValue = 5;
  coords: [number, number] = [44.505, -0.09];

}
