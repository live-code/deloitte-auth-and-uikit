import { Component } from '@angular/core';

@Component({
  selector: 'app-uikit5',
  template: `
    
    <h1>Stop Propagation</h1>
    <div (click)="parentHandler()" class="bg-sky-400 p-2">
      parent
      <div (click)="childHandler()" stopPropagation class="bg-orange-400">
        child
      </div>
    </div>

    
    <h1>Url</h1>
    <button url="https://www.google.com">visit</button>

    <h1>Pad</h1>
    <div [appPad]="padValue">
      padding <span appHighlight>example</span>
    </div>

    <h1>Margin</h1>
    <div [appMargin]="padValue">
      margin example
    </div>
    
    <input type="number" [(ngModel)]="padValue">
   
    <h1>Alert</h1>
    <div appAlert >
      uikit5 works!
    </div>
    
    <div [appAlert]="value">
      uikit5 works!
    </div>

    <div appAlert="fail">
      uikit5 works!
    </div>

    

  `,
})
export class Uikit5Component {
  value: 'success' = 'success'
  padValue: 1 = 1;

  parentHandler() {
    console.log('parent')
  }

  childHandler() {
    console.log('child')
  }
}
