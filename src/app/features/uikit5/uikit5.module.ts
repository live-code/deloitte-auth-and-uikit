import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { Uikit5RoutingModule } from './uikit5-routing.module';
import { Uikit5Component } from './uikit5.component';


@NgModule({
  declarations: [
    Uikit5Component
  ],
  imports: [
    CommonModule,
    Uikit5RoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class Uikit5Module { }
