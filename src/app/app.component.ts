import { Component } from '@angular/core';
import { map } from 'rxjs';
import { AuthenticationService } from './core/authentication.service';

@Component({
  selector: 'app-root',
  template: `
    
  <app-navbar></app-navbar>
    
   
    <hr>
    <router-outlet></router-outlet>
    
  `,
})
export class AppComponent {

/*
    this.authSrv.displayName$
      .subscribe(text => {
        console.log(text)
      })*/
}
