import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth.guard';

const routes: Routes = [
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  {
    path: 'products',
    loadChildren: () => import('./features/products/products.module').then(m => m.ProductsModule),
    canActivate: [AuthGuard]
  },
  { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'uikit1', loadChildren: () => import('./features/uikit1/uikit1.module').then(m => m.Uikit1Module) },
  { path: 'uikit2', loadChildren: () => import('./features/uikit2/uikit2.module').then(m => m.Uikit2Module) },
  { path: 'uikit3', loadChildren: () => import('./features/uikit3/uikit3.module').then(m => m.Uikit3Module) },
  { path: 'uikit4', loadChildren: () => import('./features/uikit4/uikit4.module').then(m => m.Uikit4Module) },
  { path: 'uikit5', loadChildren: () => import('./features/uikit5/uikit5.module').then(m => m.Uikit5Module) },
  { path: 'uikit6', loadChildren: () => import('./features/uikit6/uikit6.module').then(m => m.Uikit6Module) },
  { path: 'uikit7', loadChildren: () => import('./features/uikit7-pipes/uikit7-pipes.module').then(m => m.Uikit7PipesModule) },
  { path: 'uikit8', loadChildren: () => import('./features/uikit8-pipes/uikit8-pipes.module').then(m => m.Uikit8PipesModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
