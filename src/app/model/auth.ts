export type Role = 'admin' | 'guest';

export interface Auth {
  token: string;
  role: Role
  displayName: string;
}
