import { Component, EventEmitter, Input, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
     
    <div class="m-2 border border-slate-700">
      <div 
        class="bg-slate-900 text-white p-3 flex items-center justify-between"
        [ngClass]="{
          'bg-green-500': variant === 'success',
          'bg-red-500': variant === 'failed'
        }"
        (click)="toggle()"
      >
        <div>{{myTitle}}</div>
        <i (click)="iconClick.emit()" 
           stopPropagation
           *ngIf="icon" [class]="icon"></i>
      </div>
      
      <div *ngIf="isOpened">
        <ng-content select="app-card-body"></ng-content>
        <ng-content select="app-card-footer"></ng-content>
      </div>
        
    </div>
  `,
})
export class CardComponent {
  @Input({ required: true, alias: 'title'}) myTitle: string | undefined;
  @Input() icon: string | undefined;
  @Input() variant: 'success' | 'failed' | undefined;
  @Input() url: string | undefined;
  @Output() iconClick = new EventEmitter()

  @Input() isOpened = false;
  @Output() isOpenedChange = new EventEmitter<boolean>()

  toggle() {
    this.isOpened = !this.isOpened
    this.isOpenedChange.emit(this.isOpened)
  }

}
