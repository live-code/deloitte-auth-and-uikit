import { Component } from '@angular/core';

@Component({
  selector: 'app-card-body',
  template: `
    <div class="p-3">
      <ng-content></ng-content>
    </div>
  `,
})
export class CardBodyComponent {

}
