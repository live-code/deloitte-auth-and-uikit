import { Component } from '@angular/core';

@Component({
  selector: 'app-card-footer',
  template: `
    <div class="border-t-2">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class CardFooterComponent {

}
