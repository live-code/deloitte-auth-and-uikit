import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StopPropagationDirective } from '../../directives/stop-propagation.directive';
import { SharedModule } from '../../shared.module';
import { CardComponent } from './card.component';
import { CardTitleComponent } from './card-title.component';
import { CardBodyComponent } from './card-body.component';
import { CardFooterComponent } from './card-footer.component';



@NgModule({
  declarations: [
    CardComponent,
    CardTitleComponent,
    CardBodyComponent,
    CardFooterComponent
  ],
  imports: [
    CommonModule,
    StopPropagationDirective
  ],
  exports: [
    CardComponent,
    CardTitleComponent,
    CardBodyComponent,
    CardFooterComponent
  ]
})
export class CardModule { }
