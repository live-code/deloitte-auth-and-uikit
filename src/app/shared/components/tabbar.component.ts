import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `
    
    <div class="flex gap-2">
      <button
        *ngFor="let item of items"
        (click)="tabClickHandler(item)"
        [ngClass]="{'active': item.id === selectedItem?.id}"
        >{{item[labelField]}}</button>
    </div>
  `,
})
export class TabbarComponent<T extends { id: number, [key: string]: any}> {
  @Input() items: T[] = []
  @Input() selectedItem: T | undefined;
  @Output() tabClick = new EventEmitter<T>()
  @Input() labelField = 'name'
  tabClickHandler(item: T) {
    this.tabClick.emit(item);
  }
}

