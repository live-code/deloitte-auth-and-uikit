import { LatLngExpression } from 'leaflet';
import * as L from 'leaflet';
import { AfterViewInit, Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-leaflet',
  template: `
    <div #host class="map"></div>
  `,
  styles: [`
    .map { height: 180px}
  `]
})
export class LeafletComponent implements OnChanges {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLElement>
  @Input({ required: true }) coords!: LatLngExpression;
  @Input() zoom: number = 10;
  map!: L.Map;


  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', this.coords)
    if (!this.map) {
      this.init()
    }
    if(changes['zoom']) {
      this.map.setZoom(this.zoom)
    }
    if(changes['coords']) {
      this.map.setView(this.coords)
    }
  }

  ngOnInit() {
    console.log('int map')
  }

  init() {
    this.map = L.map(this.host.nativeElement)
      .setView(this.coords, this.zoom);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);

  }
}
