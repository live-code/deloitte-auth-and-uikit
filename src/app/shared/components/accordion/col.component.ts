import { Component, HostBinding, Input } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'app-col',
  template: `
      <ng-content></ng-content>
  `,
})
export class ColComponent {
  @HostBinding() get className() {
    return 'col-' + this.parent.mq
  }

  constructor(private parent: RowComponent) {
  }


}
