import { AfterContentInit, Component, ContentChild, ContentChildren, QueryList, ViewChildren } from '@angular/core';
import { PanelComponent } from './panel.component';

@Component({
  selector: 'app-accordion',
  template: `
    <div class="border-4 border-black">
      <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent implements AfterContentInit {
  @ContentChildren(PanelComponent) panels!: QueryList<PanelComponent>

  ngAfterContentInit() {
    this.panels.first.opened = true;

    this.panels.toArray().forEach(p => {
      p.toggle.subscribe(() => {
        this.closeAll();
        p.opened = true;
      })
    })
  }

  closeAll() {
    this.panels.toArray().forEach(p => {
      p.opened = false;
    })
  }
}
