import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-fx',
  template: `
    <div 
      class="flex"
      [ngClass]="{
        'justify-between': justify === 'between',
        'justify-center': justify === 'center',
        'gap-2': gap === 'sm',
        'gap-8': gap === 'lg'
      }"
    >
      <ng-content></ng-content>
    </div>
  `,
})
export class FxComponent {
  @Input() justify: 'between' | 'center' | undefined;
  @Input() gap: 'sm' | 'lg' | undefined;
}
