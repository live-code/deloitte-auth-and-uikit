import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-panel',
  animations: [
    trigger('collapse', [
      state('opened', style({
        height: '*',
        padding: 10
      })),
      state('closed', style({
        height: 0,
        padding: 0
      })),
      transition('closed <=> opened', [
        animate('0.6s cubic-bezier(0.77, 0, 0.175, 1)')
      ]),
    ])
  ],
  template: `
    <div>
      <div 
        class="title" 
        (click)="toggle.emit()">
        {{title}}
      </div>
      <div class="body" [@collapse]="opened ? 'opened' : 'closed'" >
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
    .title {
      @apply bg-slate-500 text-white p-2
    }
    .body {
      @apply border border-slate-500 p-2;
      overflow: hidden;
    }
  `]
})
export class PanelComponent {
  @Input() title: string | undefined;
  @Input() opened: boolean = false;
  @Output() toggle = new EventEmitter()

}
