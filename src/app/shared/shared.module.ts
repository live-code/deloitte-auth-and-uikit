import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from './components/card/card.module';
import { TabbarComponent } from './components/tabbar.component';
import { LeafletComponent } from './components/leaflet.component';
import { PanelComponent } from './components/accordion/panel.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { RowComponent } from './components/accordion/row.component';
import { ColComponent } from './components/accordion/col.component';
import { FxComponent } from './components/accordion/fx.component';
import { MarginDirective } from './directives/margin.directive';
import { PadDirective } from './directives/pad.directive';
import { AlertDirective } from './directives/alert.directive';
import { HighlightDirective } from './directives/highlight.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { MyRouterLinkActiveDirective } from './directives/my-router-link-active.directive';
import { IsLoggedDirective } from './directives/is-logged.directive';
import { IsSigninDirective } from './directives/is-signin.directive';
import { IfRoleIsDirective } from './directives/if-role-is.directive';
import { LoaderDirective } from './directives/loader.directive';
import { RepeaterDirective } from './directives/repeater.directive';
import { UsersRepeaterDirective } from './directives/userRepeater.directive';
import { DoublePipe } from './pipes/double.pipe';
import { MemoryPipe } from './pipes/memory.pipe';
import { MapquestPipe } from './pipes/mapquest.pipe';
import { TimesagoPipe } from './pipes/timesago.pipe';
import { IntToArrayPipe } from './pipes/int-to-array.pipe';
import { FilterByGenderPipe } from './pipes/filter-by-gender.pipe';
import { FilterByNamePipe } from './pipes/filter-by-name.pipe';
import { SortPipe } from './pipes/sort.pipe';



@NgModule({
  declarations: [
    TabbarComponent,
    LeafletComponent,
    PanelComponent,
    AccordionComponent,
    RowComponent,
    ColComponent,
    FxComponent,
    PadDirective,
    AlertDirective,
    MarginDirective,
    HighlightDirective,
    UrlDirective,

    MyRouterLinkActiveDirective,
    IsLoggedDirective,
    IsSigninDirective,
    IfRoleIsDirective,
    LoaderDirective,
    RepeaterDirective,
    UsersRepeaterDirective,
    DoublePipe,
    MemoryPipe,
    MapquestPipe,
    TimesagoPipe,
    IntToArrayPipe,
    FilterByGenderPipe,
    FilterByNamePipe,
    SortPipe
  ],
  imports: [
    CardModule,
    CommonModule,
    StopPropagationDirective,
  ],
  exports: [
    CardModule,
    TabbarComponent,
    LeafletComponent,
    PanelComponent,
    AccordionComponent,
    RowComponent,
    ColComponent,
    FxComponent,
    PadDirective,
    AlertDirective,
    MarginDirective,
    HighlightDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkActiveDirective,
    IsLoggedDirective,
    IsSigninDirective,
    IfRoleIsDirective,
    LoaderDirective,
    RepeaterDirective,
    UsersRepeaterDirective,
    DoublePipe,
    MemoryPipe,
    MapquestPipe,
    TimesagoPipe,
    IntToArrayPipe,
    FilterByGenderPipe,
    FilterByNamePipe,
    SortPipe
  ]
})
export class SharedModule { }
