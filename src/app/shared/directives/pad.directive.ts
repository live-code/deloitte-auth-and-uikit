import { Directive, ElementRef, HostBinding, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appPad]'
})
export class PadDirective {
  @Input() set appPad(val: 1 | 2 | 3) {
    // this.el.nativeElement.style.margin = (10 * val) + 'px'
    this.renderer.setStyle(
      this.el.nativeElement,
      'margin',
      (10 * val) + 'px'
    )

    this.renderer.setAttribute(
      this.el.nativeElement,
      'alt',
      'padding ' + val
    )
  }

  constructor(
     private el: ElementRef<HTMLElement>,
     private renderer: Renderer2
  ) {

  }
/*
  @HostBinding() get class() {
    return this.appPad < 3 ? 'p-4' : 'p-10'
  }*/


}
