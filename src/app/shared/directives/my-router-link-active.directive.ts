import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, interval, Subject, Subscription, takeUntil } from 'rxjs';

@Directive({
  selector: '[appMyRouterLinkActive]'
})
export class MyRouterLinkActiveDirective {
  @Input({ alias: 'appMyRouterLinkActive', required: true }) classToApply!: string
  destroy$ = new Subject();

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    private router: Router
  ) {
    const routerLink = el.nativeElement.getAttribute('routerLink');

    router.events
      .pipe(
        takeUntil(this.destroy$),
        filter(ev => ev instanceof NavigationEnd)
      )
      .subscribe(ev => {
        if (ev instanceof NavigationEnd) {
          if (routerLink && ev.url.includes(routerLink)) {
            this.renderer.addClass(el.nativeElement, this.classToApply)
          } else {
            this.renderer.removeClass(el.nativeElement, this.classToApply)
          }
        }
      })
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }

}
