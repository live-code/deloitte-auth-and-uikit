import { Directive, ElementRef, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[url]'
})
export class UrlDirective {
  @Input() url: string | undefined;
  @HostBinding('style.cursor') cursor = 'pointer'

  @HostListener('click')
  openUrl() {
    window.open(this.url)
  }

}
