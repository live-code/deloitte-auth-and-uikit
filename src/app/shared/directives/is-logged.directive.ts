import { Directive, ElementRef, TemplateRef, ViewContainerRef } from '@angular/core';
import { distinct, distinctUntilChanged } from 'rxjs';
import { AuthenticationService } from '../../core/authentication.service';

@Directive({
  selector: '[appIsLogged]'
})
export class IsLoggedDirective {

  constructor(
    private el: ElementRef<HTMLElement>,
    private tpl: TemplateRef<any>,
    private authService: AuthenticationService,
    private view: ViewContainerRef
  ) {
    this.authService.isLogged$
      .subscribe(isLogged => {
        if (isLogged) {
          view.createEmbeddedView(this.tpl)
        } else {
          view.clear();
        }
      })
  }

}
