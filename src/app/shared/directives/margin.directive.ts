import { Directive, ElementRef, HostBinding, Input, OnChanges, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appMargin]'
})
export class MarginDirective implements OnChanges {
  @Input({ alias: 'appMargin'}) value: 1 | 2 | 3 = 1;

  constructor(
     private el: ElementRef<HTMLElement>,
     private renderer: Renderer2
  ) {}

  ngOnChanges() {
    this.renderer.setStyle(
      this.el.nativeElement,
      'margin',
      (10 * this.value) + 'px'
    )
  }


}
