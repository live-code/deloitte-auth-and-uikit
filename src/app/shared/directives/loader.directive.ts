import { ComponentRef, Directive, EventEmitter, Input, OnInit, Output, Type, ViewContainerRef } from '@angular/core';
import { COMPONENTS_MAP, Config, WidgetList, WidgetType } from '../../features/uikit6/uikit6.component';
import { Auth } from '../../model/auth';
import { CardComponent } from '../components/card/card.component';
import { LeafletComponent } from '../components/leaflet.component';

@Directive({
  selector: '[appLoader]'
})
export class LoaderDirective implements OnInit{
  @Input({ alias: 'appLoader'}) config!: Config;
  @Output() doAction = new EventEmitter<{evt: string, params: any}>()

  constructor(private view: ViewContainerRef) {}

  ngOnInit() {
    const compo: ComponentRef<any> =
      this.view.createComponent(COMPONENTS_MAP[this.config.type])

    for (let key in this.config.data) {
      compo.setInput(key, this.config.data[key])
    }

    this.config.events?.forEach((evt: string) => {
      compo.instance[evt].subscribe((params: any) => {
        this.doAction.emit({
          evt,
          params
        })
      })
    })
  }

}

