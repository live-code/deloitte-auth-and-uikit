import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appAlert]'
})
export class AlertDirective {
  @HostBinding() get class() {
    const cls = 'p-2 text-white rounded-xl m-2';
    switch(this.appAlert) {
      case 'success':
        return cls + ' bg-green-700';
      case 'fail':
        return cls + ' bg-red-700';
      default:
        return cls + ' bg-sky-700'
    }
  }

  @Input() appAlert: 'success' | 'fail' | '' = '';

}


