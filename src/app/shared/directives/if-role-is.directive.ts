import { Directive, ElementRef, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { combineLatest, Subscription, withLatestFrom } from 'rxjs';
import { AuthenticationService } from '../../core/authentication.service';
import { Role } from '../../model/auth';

@Directive({
  selector: '[appIfRoleIs]'
})
export class IfRoleIsDirective implements OnInit {
  @Input() appIfRoleIs!: Role;
  sub!: Subscription;

  constructor(
    private el: ElementRef<HTMLElement>,
    private tpl: TemplateRef<any>,
    private authService: AuthenticationService,
    private view: ViewContainerRef
  ) { }

  ngOnInit() {
    // solution 1: check role only
    /*this.sub = this.authService.role$
      .subscribe(loggedRole => {
        if (loggedRole === this.appIfRoleIs) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear();
        }
      })*/

    // solution 2: check if Logged and role
    /*
    this.sub = this.authService.isLogged$
      .pipe(
        withLatestFrom(this.authService.role$)
      )
      .subscribe(([isLogged, role]) => {
        if (isLogged && role === this.appIfRoleIs) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear();
        }
      })
     */

    this.sub = combineLatest([
      this.authService.isLogged$,
      this.authService.role$,
      // obs3
    ])
      .subscribe(([isLogged, role]) => {
        if (isLogged && role === this.appIfRoleIs) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear();
        }
      })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
