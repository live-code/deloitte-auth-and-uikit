import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { User } from '../../features/uikit6/uikit6.component';

export type ContextType = {
  total: number,
  index: number,
  $implicit: User & { displayName: string}
}

@Directive({
  selector: '[usersRepeater]'
})
export class UsersRepeaterDirective {

  @Input() set usersRepeaterOf(data: User[]) {

    data.forEach((item, i) => {
      this.view.createEmbeddedView<ContextType>(
        this.tpl,
        {
          total: data.length,
          index: i+1,
          $implicit: {
            ...item,
           displayName: `${item.name} ${item.surname}`
          }
        }
      )
    })
  }

  constructor(
    private tpl: TemplateRef<ContextType>,
    private view: ViewContainerRef
  ) {

  }
}

