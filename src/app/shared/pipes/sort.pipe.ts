import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../features/uikit8-pipes/uikit8-pipes.component';

@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {


  transform(users: User[], order: 'ASC' | 'DESC'): User[] {
    const sorting = users.sort((a, b) => a.name.localeCompare(b.name));
    return order === 'ASC' ? sorting : sorting.reverse()
  }

}
