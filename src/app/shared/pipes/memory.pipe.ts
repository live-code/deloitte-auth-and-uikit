import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'memory'
})
export class MemoryPipe implements PipeTransform {

  transform(value: number, formatTo: 'mb' | 'byte'): string {
    switch (formatTo) {
      case 'mb':
        return `${value * 1000} megabytes`

      case 'byte':
        return `${value * 1000000} bytes`
    }
  }

}
