import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapquest'
})
export class MapquestPipe implements PipeTransform {

  transform(city: string, w?: number | null, h?: number | null): unknown {
    return `https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=${city}&size=${w || 600},${h || 300}&zoom=10`;
  }

}
