import { Pipe, PipeTransform } from '@angular/core';
import { Gender, User } from '../../features/uikit8-pipes/uikit8-pipes.component';

@Pipe({
  name: 'filterByGender'
})
export class FilterByGenderPipe implements PipeTransform {

  transform(users: User[], gender: Gender): User[] {
    if (gender === 'all') {
      return users;
    }
    return users.filter(u => {
      return u.gender === gender
    });
  }

}
