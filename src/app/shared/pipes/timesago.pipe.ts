import { Pipe, PipeTransform } from '@angular/core';
import { formatDistance } from 'date-fns';

@Pipe({
  name: 'timesago'
})
export class TimesagoPipe implements PipeTransform {

  transform(date: Date | number): string {
    return formatDistance(date, new Date(), { addSuffix: true })
  }

}
