import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../features/uikit8-pipes/uikit8-pipes.component';

@Pipe({
  name: 'filterByName'
})
export class FilterByNamePipe implements PipeTransform {

  transform(users: User[], textToFind: string): User[] {
    return users.filter(u => {
      return u.name.toLowerCase().includes(textToFind.toLowerCase())
    });
  }

}
