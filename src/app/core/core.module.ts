import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink, RouterLinkActive, RouterModule } from '@angular/router';
import { MyRouterLinkActiveDirective } from '../shared/directives/my-router-link-active.directive';
import { SharedModule } from '../shared/shared.module';
import { NavbarComponent } from './navbar.component';



@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    CommonModule,
    RouterLink,
    RouterLinkActive,
    SharedModule

  ],
  exports: [
    NavbarComponent
  ]
})
export class CoreModule { }
