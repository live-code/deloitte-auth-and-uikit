import { Component } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div class="flex flex-wrap gap-2 items-center m-2">
      <button [routerLink]="'login'" routerLinkActive="accent">login</button>
      <button routerLink="products"
              *appIfRoleIs="'admin'">products</button>
      <button routerLink="home" 
              appMyRouterLinkActive="accent">home</button>
      <button routerLink="uikit1" routerLinkActive="accent">uikit1</button>
      <button routerLink="uikit2" routerLinkActive="accent">uikit2</button>
      <button routerLink="uikit3" routerLinkActive="accent">uikit3</button>
      <button routerLink="uikit4" routerLinkActive="accent">uikit4</button>
      <button routerLink="uikit5" routerLinkActive="accent">uikit5</button>
      <button routerLink="uikit6" routerLinkActive="accent">uikit6</button>
      <button routerLink="uikit7" routerLinkActive="accent">uikit7 - pipes</button>
      <button routerLink="uikit8" routerLinkActive="accent">uikit8 - pipes</button>

      <!--<button *ngIf="authSrv.isLogged$ | async" (click)="authSrv.logout()">Logout</button>-->
      <button *appIsLogged (click)="authSrv.logout()">Logout</button>

      {{ authSrv.displayName$ | async}}
    </div>

  `,
  styles: [
  ]
})
export class NavbarComponent {
  constructor(public authSrv: AuthenticationService) {}
}
