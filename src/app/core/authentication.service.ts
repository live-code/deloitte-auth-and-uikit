import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, distinctUntilChanged, map, Subject, tap } from 'rxjs';
import { Auth } from '../model/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  auth$ = new BehaviorSubject<Auth | null>(null)

  constructor(private http: HttpClient) {
    const fromLocalStorage = localStorage.getItem('auth');

    if (fromLocalStorage) {
      const auth: Auth = JSON.parse(fromLocalStorage)
      this.auth$.next(auth)
    }

  }

  login(username: string, password: string) {
    this.http.get<Auth>('http://localhost:3000/login')
      .subscribe(res => {
        this.auth$.next(res);
        localStorage.setItem('auth', JSON.stringify(res))
      })
  }
  logout() {
    localStorage.removeItem('auth');
    this.auth$.next(null);

  }

  get isLogged$() {
    return this.auth$
      .pipe(
        map(res => !!res),
        distinctUntilChanged(),
      )
  }

  get displayName$() {
    return this.auth$
      .pipe(
        map(res => res?.displayName)
      )
  }

  get role$() {
    return this.auth$
      .pipe(
        map(res => res?.role)
      )
  }

  get token$() {
    return this.auth$
      .pipe(
        map(res => res?.token)
      )
  }

}

