import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { first, map, mergeMap, Observable, of, take, tap } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private authSrv: AuthenticationService,
    private router: Router,
    private http: HttpClient
  ) {

  }


  canActivateASYNC(): Observable<boolean> {
    return this.authSrv.token$
      .pipe(
        // first(),
        take(1),
        mergeMap(token => this.http.get<{response: string}>('http://localhost:3000/validate', {
          headers: {
            Authorization: 'Bearer ' + token
          }
        })),
        map(res => res.response === 'ok'),
        tap((isValid) => {
          if (!isValid) {
            this.authSrv.logout();
            this.router.navigateByUrl('/login')
          }
        })
      )

    /*
    this.http.get<{response: string}>('http://localhost:3000/validate', {
      headers: {
        Authorization: 'Bearer ' + TOKEN
      }
    })
      .pipe(
        map(res => res.response === 'ok'),
        tap((isValid) => {
          if (!isValid) {
            this.authSrv.logout();
            this.router.navigateByUrl('/login')
          }
        })
      )

     */
  }

  canActivate(): Observable<boolean> {
    return this.authSrv.isLogged$
      .pipe(
        tap((res) => {
          if (!res) {
            this.router.navigateByUrl('login')
          }
        })
      )
  }
}
