import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, first, iif, mergeMap, Observable, of, tap, throwError, withLatestFrom } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationService, private router: Router) {}


  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    return this.authService.isLogged$
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        mergeMap(([isLogged, token]) => {
          return iif(
            () => isLogged && !!token,
            next.handle(request.clone({ setHeaders: { Auth: 'Bearer' + token } })),
            next.handle(request)
          )
        }),
        catchError(err => {
          // console.log('inter error')
          if (err instanceof HttpErrorResponse) {
            switch(err.status) {
              default:
              case 404:
                this.authService.logout();
               // this.router.navigateByUrl('/login')
                break;
              case 401:
                break;
            }
          }
          return throwError(err)
        })
      )
  }

  intercept3(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
      .pipe(
        catchError(err => {
          return of(err)
        })
      )
  }

  intercept2(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.isLogged$
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        mergeMap(([isLogged, token]) => {
          if (isLogged && token) {
            const cloned = request.clone({
              setHeaders: {
                Authorization: token
              }
            })
            return next.handle(cloned)
          }
          return next.handle(request)
        })
      )
  }
}
